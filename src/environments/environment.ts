// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  loginCreds: {
    email: {
      grant_type: 'password',
      client_id: 5,
      // client_secret: 'ofA4mW8IxuKxMOXgCoXFmtxhgI7Zb7uyhp8dV8Ha', //local
      client_secret: 'aqAYwl1lwkyQ0iWlM5eqjbi7C2C9H1srJrv3vKOX', //production
      scope: ''
    },
    social: {
      // client_secret: 'VOO6YmBwiP14sRCQvfhC9vQghwFxeHwsShbsx9i5' //local
      client_secret: 'aeQzAo5OcUdR4ZTmEZpRUj3tkxprl5Lkq1GK9kPf' //production
    }
  },
  api: {
    ssl: true,
    //host: '192.168.1.5',
    //host: '192.168.1.7',
    //host: '192.168.1.5',
    // host: '192.168.1.7',
   // host: 'api.socksbakery.com', //production
    host: 'socksbakery.com/laravel_api/public',
   // host: 'localhost',
    //port: 8000,
    // host: 'localhost',
    // port: 8000,
    port: 80,
    prefix: 'oauth'
  },
  imageEnd: {
    ssl: true,
    //host: '192.168.1.5',
    //host: '192.168.1.7',
    //host: '192.168.1.5',
    // host: '192.168.1.7',
    host: 'admin.socksbakery.com', //production
    //host: 'localhost',
    //port: 8012,
    port: 80,
    prefix: 'oauth'
  },
  Razorpay: {
    // key_id: 'rzp_live_pNInU4gqlcYqQa',//tsb
    key_id: 'rzp_live_Q8mpgfFUZzCDmJ',
    name: 'SockSoho',
    description: 'SockSoho Purchase Description',
    image: 'assets/img/logo-symbol-black.png',
    theme_color: '#212529'
  },
  cryptoJs: {
    secretKey: 'k3ep6TM8lRzvTQxETgvqFUGG'
  },
  services: {
    paypal: {
      // production: 'AR71NTXRDXJumvgwXUsdRRs6n3jg6xsRuTkZXH11ZK0FYU8OGvkSdKjbUXwqydWgX_0l7IaGsOphruQ3', //tsb
      // sandbox: 'AT5Kv6F3KE26l9piC4hsPVtSNzYAgfG6WkQqnkv1t4UaRR0Zp6rc1lWjfhdablDhJ32vP6K-wGFExyO6', //tsb
      production: 'AcA-byU-q68RFs6tQRo_Q97YKv0xHiDtGjoaq525nApUe-1Ae4bYyS0Ajf3JjGUSCrQxBwUwoxPsGTyA',
      sandbox: 'AdCR9sNtpfFGwL0yRxw0B3JF0hfj_jzJKfLXd_D-cm8itl4o0O3wq3RUPQ8VtdXqnUNx1uNS_HI7posp',
      env: 'production'
    },
  }
};
