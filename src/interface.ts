export interface MyCart {
    info: {
        subTotalUSD: number, 
        subTotalINR: number
    },
    items: [{
        product_id: string,
        size: string,
        quantity: number,
        priceUSD: number,
        priceINR: number,
        edition: string,
        product_title: string,
        imgSrc: string,
        totalPriceUSD: number,
        totalPriceINR: number,
        availSizes: Array<any>
    }]
}

export interface SelectedProduct {
    product_id: string,
    size: string,
    quantity: number,
    quantity_id: number,
    priceUSD: number,
    priceINR: number,
    edition: string,
    product_title: string,
    imgSrc: string,
    totalPriceUSD: number,
    totalPriceINR: number,
    availSizes: Array<any>,
    size_id: string
}

export interface ProductDetails {
    images: Array<any>,
    image_directory_url: string,
    actual_priceUSD: number,
    actual_priceINR: number,
    sub_category_name: string,
    version_name: string,
    product_id: string
}

export interface userDetails {
    name: string,
    email: string
}
