import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import * as toastr from 'toastr';

@Injectable()
export class ForgotPasswordResolveService implements Resolve<any> {

  constructor(
    private auth: AuthenticationService,
    private router: Router
  ) {}

  resolve(router: ActivatedRouteSnapshot): Promise<any> {
    let id = router.params.id;
    return new Promise((resolve, reject) => {
      this.auth.verifyToken(id)
      .then(res => {
        resolve(res)
      })
      .catch(err => {
        toastr.error('This is not a valid link')
        this.router.navigateByUrl('/');
        reject(err)
      })
    })
  }

}
