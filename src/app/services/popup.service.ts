import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';

@Injectable()
export class PopupService {

  constructor(
  		private storage: StorageService
  ) { }

  public getPopupStatus(){
  	let popUpStatus = JSON.parse(this.storage.get('popUpStatus'));
  	return popUpStatus;
  }

  public setPopupStatus(status){
  	this.storage.set('popUpStatus',JSON.stringify(status));
  }

}
