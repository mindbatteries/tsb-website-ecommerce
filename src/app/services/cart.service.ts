import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { StorageService } from './storage.service';
import { MyCart } from '../../interface';
import { SelectedProduct } from '../../interface';

@Injectable()
export class CartService {

  private myCart: MyCart;
  private initializeMyCart;

  constructor(
    private storage: StorageService
  ) {
    this.initializeMyCart = { info: {subTotal: 0}, items: [] };
    this.myCart = this.getCart() || this.initializeMyCart;
  }

  myCartSubject = new BehaviorSubject(this.getCart() || this.initializeMyCart);

  addToCart(item: SelectedProduct) {
    let item_no = 0;
    let signal = false;
    let att = '';
    let quantity = 0;
    this.myCart.items.map((value,index) => {
      if(value.size == item.size && value.product_id == item.product_id){
        att = 'plus';
        quantity = item.quantity;
        signal = true; 
        this.changeQuantity(att,index,quantity);
        return;
      }
    });
    if(signal){
        return;
    }
    this.myCart.items.push(item);
    this.subTotal();
    this.setCart();
    this.myCartSubject.next(this.getCart());
  }

  changeQuantity(att, index, quantity) {
    let data;
    let value = this.myCart.items[index].quantity;
    value = value || 0;
    if(att == 'plus') {
        data = { attribute: 'quantity', value: value + quantity };
    }
    if(data.value != 0){
      this.updateCart(index, data);
    }
  }

  removeFromCart(index) {
    this.myCart.items.splice(index, 1);
    this.subTotal();
    this.setCart();
    this.myCartSubject.next(this.getCart());
  }

  updateCart(index, data) {
    switch (data.attribute) {
      case 'quantity':
        this.myCart.items[index].quantity = data.value;
        this.itemTotalPrice(index);
        this.subTotal();
        break;

      case 'size':
        this.myCart.items[index].size = data.value;
        break;
    }
    this.setCart();
    this.myCartSubject.next(this.getCart());
  }

  itemTotalPrice(index) {
    this.myCart.items[index].totalPriceUSD = this.myCart.items[index].priceUSD * this.myCart.items[index].quantity;
    this.myCart.items[index].totalPriceINR = this.myCart.items[index].priceINR * this.myCart.items[index].quantity;
  }

  subTotal() {
    let subTotalUSD: number = 0;
    let subTotalINR: number = 0;
    this.myCart.items.forEach((item, index) => {
      subTotalUSD += item.totalPriceUSD;
      subTotalINR += item.totalPriceINR;
    })
    this.myCart.info.subTotalUSD = subTotalUSD;
    this.myCart.info.subTotalINR = subTotalINR;
  }

  getCart() {
    return JSON.parse(this.storage.get('cart'));
  }

  setCart() {
    this.storage.set('cart', JSON.stringify(this.myCart));
  }

  emptyCart(){
    this.storage.remove('cart');
  }

  cartStatus() {
    this.myCartSubject.next(this.getCart() || this.initializeMyCart);
    return this.myCartSubject.asObservable();
  }

}
