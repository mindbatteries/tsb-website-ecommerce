import { TestBed, inject } from '@angular/core/testing';

import { ForgotPasswordResolveService } from './forgot-password-resolve.service';

describe('ForgotPasswordResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ForgotPasswordResolveService]
    });
  });

  it('should be created', inject([ForgotPasswordResolveService], (service: ForgotPasswordResolveService) => {
    expect(service).toBeTruthy();
  }));
});
