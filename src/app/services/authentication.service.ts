import { Injectable } from '@angular/core';
import { ApiContractService } from './api-contract.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router, RouterLink } from '@angular/router';
import { StorageService } from './storage.service';
import { CartService } from './cart.service';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthenticationService {

  constructor(
    private api: ApiContractService,
    private storage: StorageService,
    private cart: CartService,
    private router: Router
  ) { }

  loggedInSubject = new BehaviorSubject<boolean>(!!(this.storage.get('current_user') && (new Date().getTime() < parseInt(this.storage.get('validTill')))));

  register(obj): Promise<any> {
    return new Promise((resolve, reject) => {
      let data = {
        name: obj.name,
        email: obj.email,
        newsletter_subscribed: obj.newsletter_subscribed,
        password: obj.password
      }
      this.api.post('api/register', data)
        .then(res => {
          resolve(res)
        })
        .catch(err => reject(err))

    })
  }

  login(obj, type): Promise<any> {
    return new Promise((resolve, reject) => {
      let data = {
        username: obj.email,
        password: obj.password,
        grant_type: environment.loginCreds.email.grant_type,
        client_id: environment.loginCreds.email.client_id,
        client_secret: environment.loginCreds.email.client_secret,
        scope: environment.loginCreds.email.scope
      }
      this.api.post('oauth/token', data)
        .then(res => {
          this.createSession(res)
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })
  }

  socialLogin(token, platform): Promise<any> {
    let data = {
      grant_type: 'social',
      client_id: 3,
      client_secret: environment.loginCreds.social.client_secret,
      provider: platform,
      access_token: token
    }
    return new Promise((resolve, reject) => {
      this.api.post('oauth/token', data)
      .then(res => {
        this.createSession(res)
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
    })
  }

  createSession(data) {
    this.storage.set('token', data.data.access_token);
    this.storage.set('refreshToken', data.data.refresh_token);
    this.storage.set('current_user', JSON.stringify(data.data.user_details));
    this.storage.set('validTill', new Date().getTime() + (data.data.expires_in * 1000));
    this.loggedInSubject.next(true);
  }

  loggedIn() {
    return this.loggedInSubject.asObservable();
  }

  isActiveUser(): boolean {
    return !!(this.storage.get('current_user') && (new Date().getTime() < parseInt(this.storage.get('validTill'))));
  }

  async logout() {
    await this.storage.clear();
    this.loggedInSubject.next(false);
    this.cart.cartStatus();
    this.router.navigate(['/']);
  }

  forgotPassword(email): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('api/password/reset', { email })
        .then(resolve)
        .catch(reject)
    })
  }

  newPassword(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('api/password/new', data)
        .then(resolve)
        .catch(reject)
    })
  }

  verifyToken(token): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('api/password/verify', { token })
        .then(resolve)
        .catch(reject)
    })
  }

  getCurrentUser() {
    return JSON.parse(this.storage.get('current_user'));
  }


}
