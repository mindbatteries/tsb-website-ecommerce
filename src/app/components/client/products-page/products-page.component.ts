import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { ApiContractService } from '../../../services/api-contract.service';

@Component({
  selector: 'app-products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.css']
})
export class ProductsPageComponent implements OnInit {

    public productList = [];

  constructor(
      private api: ApiService,
      private apiCont: ApiContractService
  ) { }

  ngOnInit() {
      this.api.getProducts()
      .then(res => {
          let url;
          res.data.forEach((item, index) => {
            item.image = this.apiCont.buildUrl2(item.image)
          })
          this.productList = res.data;
          this.productList.splice(1, 1);
      })
      .catch();
  }

}
