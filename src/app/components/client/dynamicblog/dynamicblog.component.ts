import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from '../../../services/api.service';
import { ApiContractService } from '../../../services/api-contract.service';

@Component({
  selector: 'app-dynamicblog',
  templateUrl: './dynamicblog.component.html',
  styleUrls: ['./dynamicblog.component.css']
})
export class DynamicblogComponent implements OnInit {

	private activeRoute;
	public mypostData: any;

  constructor(
    private sanitizer: DomSanitizer,
  	private route: ActivatedRoute,
  	private api: ApiService,
    private apiCont: ApiContractService
  ) {
  		this.route.params.subscribe(params => {
      		this.activeRoute = params;
    	})

      this.mypostData = {};
  	}

  ngOnInit() {

  	let id = this.activeRoute.id;
  	this.api.getBlogPost(id)
      .then(res => {
        this.mypostData = res.data[0];
        this.mypostData.mydata = this.sanitizer.bypassSecurityTrustResourceUrl(this.mypostData.mydata);
        console.log(this.mypostData);
      })
      .catch()
  }

}
