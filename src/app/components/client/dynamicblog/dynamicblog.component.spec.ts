import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicblogComponent } from './dynamicblog.component';

describe('DynamicblogComponent', () => {
  let component: DynamicblogComponent;
  let fixture: ComponentFixture<DynamicblogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicblogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicblogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
