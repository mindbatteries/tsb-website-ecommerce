import { Component, OnInit } from '@angular/core';
// import * as $ from 'jquery';

@Component({
  selector: 'app-animation',
  templateUrl: './animation.component.html',
  styleUrls: ['./animation.component.css']
})
export class AnimationComponent implements OnInit {

  constructor() { }

  animation() {

    // var isScrolling = false;
    //
    // window.addEventListener("scroll", throttleScroll, false);
    //
    // function throttleScroll(e) {
    //   if (isScrolling == false) {
    //     window.requestAnimationFrame(function() {
    //       scrolling(e);
    //       isScrolling = false;
    //     });
    //   }
    //   isScrolling = true;
    // }
    //
    // document.addEventListener("DOMContentLoaded", scrolling, false);
    //
    // var div = document.querySelector("#animated-div");
    // var div2 = document.querySelector("#animated-div2");
    // var div3 = document.querySelector("#animated-div3");
    // var div4 = document.querySelector("#ditch");
    // var p1 = document.querySelector("#p1");
    // var p2 = document.querySelector("#p2");
    // var p3 = document.querySelector("#p3");
    // var p4 = document.querySelector("#p4");
    // function scrolling(e) {
    //
    //   if (isFullyVisible(div)) {
    //     p1.classList.add("animate-p1");
    //     p2.classList.add("animate-p2");
    //     p3.classList.add("animate-p3");
    //     p4.classList.add("animate-p4");
    //     div2.classList.add("turnOnLeftDiv");
    //     div3.classList.add("turnOnRightDiv");
    //
    //     p1.classList.remove("fadeout");
    //     p2.classList.remove("fadeout");
    //     p3.classList.remove("fadeout");
    //     p4.classList.remove("fadeout");
    //     div2.classList.remove("fadeout");
    //     div3.classList.remove("fadeout");
    //   }
    //   else {
    //     p1.classList.remove("animate-p1");
    //     p2.classList.remove("animate-p2");
    //     p3.classList.remove("animate-p3");
    //     p4.classList.remove("animate-p4");
    //     div2.classList.remove("turnOnLeftDiv");
    //     div3.classList.remove("turnOnRightDiv");
    //
    //     p1.classList.add("fadeout");
    //     p2.classList.add("fadeout");
    //     p3.classList.add("fadeout");
    //     p4.classList.add("fadeout");
    //     div2.classList.add("fadeout");
    //     div3.classList.add("fadeout");
    //   }
    //
    //   function isPartiallyVisible(el) {
    //     var elementBoundary = el.getBoundingClientRect();
    //
    //     var top = elementBoundary.top;
    //     var bottom = elementBoundary.bottom;
    //     var height = elementBoundary.height;
    //
    //     return ((top + height >= 0) && (height + window.innerHeight >= bottom));
    //   }
    //
    //   function isFullyVisible(el) {
    //     var elementBoundary = el.getBoundingClientRect();
    //
    //     var top = elementBoundary.top;
    //     var bottom = elementBoundary.bottom;
    //
    //     return ((top >= 0) && (bottom <= window.innerHeight));
    //   }
    // }
  }

  ngOnInit() {
    this.animation();
  }

}
