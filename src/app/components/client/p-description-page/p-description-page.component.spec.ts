import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PDescriptionPageComponent } from './p-description-page.component';

describe('PDescriptionPageComponent', () => {
  let component: PDescriptionPageComponent;
  let fixture: ComponentFixture<PDescriptionPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PDescriptionPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PDescriptionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
