import { Component, OnInit } from '@angular/core';
import { CartService } from '../../../services/cart.service';
import { ApiService } from '../../../services/api.service';
import { ApiContractService } from '../../../services/api-contract.service';
import { ProductDetails, SelectedProduct, userDetails } from '../../../../interface';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from "@angular/router";
import { StorageService } from '../../../services/storage.service';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-p-description-page',
  templateUrl: './p-description-page.component.html',
  styleUrls: ['./p-description-page.component.css']
})
export class PDescriptionPageComponent implements OnInit {

  public productDetails;
  public sizes = [];
  public actual_priceUSD: number;
  public actual_priceINR: number;
  public special_priceUSD: number;
  public special_priceINR: number;
  public selectedSize: number;
  private activeRoute;
  private selectedProduct: SelectedProduct;
  quantityForm: FormGroup;
  public recommendedProducts = [];
  public userDetails;
  subscribeForm: FormGroup;
  subscribed: boolean;

  constructor(
    private cart: CartService,
    private api: ApiService,
    private apiCont: ApiContractService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private storage: StorageService,
    public router: Router
  ) {

    this.subscribeForm = new FormGroup({
      email: new FormControl('', {
        validators: [Validators.required, Validators.email],
        updateOn: 'blur'
      })
    });

    this.subscribed = false;



    this.route.params.subscribe(params => {
      this.activeRoute = params;
    })

    this.quantityForm = this.fb.group({
      quantity: [1, Validators.required]
    })

    this.productDetails = {
      images: [{ imgSrc: '' }],
      image_directory_url: '',
      actual_priceINR: 0,
      actual_priceUSD: 0,
      sub_category_name: '',
      version_name: '',
      product_id: '',

    }

    this.userDetails = {
      name: '',
      email: '',
    }

    this.selectedSize = 0;
  }

  selectSize(index: number) {
    this.selectedSize = index;
  }

  convertPrice(price){
    let USD_rate = 1;
    let INR_rate = 70;
    let converted_price = 0;
    converted_price = (price * USD_rate)/INR_rate;
    // return converted_price.toFixed(2);
    return Math.ceil(converted_price);
  }

  addToCart() {
    //let totalPriceUSD = this.quantityForm.value.quantity * this.convertPrice(this.productDetails.actual_priceUSD);
    //alert(this.actual_priceUSD);
    let totalPriceUSD = this.quantityForm.value.quantity * this.convertPrice(this.actual_priceINR);
    //let totalPriceINR = this.quantityForm.value.quantity * this.productDetails.actual_priceINR;
    let totalPriceINR = this.quantityForm.value.quantity * this.actual_priceINR;

    this.selectedProduct = {
      product_id: this.productDetails.product_id,
      size: this.sizes[this.selectedSize].varchar_field_value,
      size_id: this.sizes[this.selectedSize].Size_id,
      quantity: parseInt(this.quantityForm.value.quantity),
      quantity_id: this.productDetails.attribute_set.Socks.Quantity[0].Quantity_id,
      priceUSD: this.convertPrice(this.actual_priceINR),
      priceINR: this.actual_priceINR,
      edition: this.productDetails.sub_category_name,
      product_title: this.productDetails.version_name,
      imgSrc: this.productDetails.images[0].image_name,
      totalPriceUSD: totalPriceUSD,
      totalPriceINR: totalPriceINR,
      availSizes: this.sizes
    }
    this.cart.addToCart(this.selectedProduct);

    if(window.innerWidth <= 786) {
      this.router.navigate(['/cart']);
   }

  }

  changeMainImg(e) {
    let src = e.target.src;
    $('#big-picture').attr('src', src);
  }

  // subscribe() {
  //   if(this.subscribeForm.valid) {
  //     this.api.subscribe(this.subscribeForm.value.email)
  //     .then(res => {
  //       this.subscribed = true;
  //       setTimeout(() => {
  //         this.subscribeForm.reset();
  //         this.subscribed = false;
  //       }, 5000)
  //     })
  //     .catch()
  //   }
  // }

  ngOnInit() {
      this.userDetails = {
        name: 'anonymous',
        email: 'anonymous'
      }
      this.userDetails = JSON.parse(this.storage.get('current_user'));
      //console.log(this.userDetails);
    this.api.getProductDetails(this.activeRoute.id)
      .then(res => {
        res.data.images.forEach((item, index) => {
          item.image_name = this.apiCont.buildUrl2(item.image_name);
        })
        this.actual_priceUSD = this.convertPrice(res.data.actual_price);
        this.actual_priceINR = res.data.actual_price;
        this.special_priceUSD = this.convertPrice(res.data.special_price);
        this.special_priceINR = res.data.special_price;
        this.sizes = res.data.attribute_set.Socks.Size;
        this.productDetails = res.data;
      })

    this.api.getAlsoLike(this.activeRoute.id)
      .then(res => {
        res.data.forEach((item, index) => {
          item.image = this.apiCont.buildUrl2(item.image);
        })
        var remove_index: number;
        this.recommendedProducts = res.data;
        this.recommendedProducts.forEach((item, index) => {
          if(item.product_id == '2'){
            remove_index = index;
          }
        })
        if(typeof remove_index !== "undefined") {
            this.recommendedProducts.splice(remove_index, 1);
        }
      })
      .catch()

  }

}
