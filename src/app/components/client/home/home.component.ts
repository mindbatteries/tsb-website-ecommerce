import { Component, OnInit } from '@angular/core';
import { CartService } from '../../../services/cart.service';
import { ApiService } from '../../../services/api.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiContractService } from '../../../services/api-contract.service';
import { ProductDetails, SelectedProduct } from '../../../../interface';
import { PopupService } from '../../../services/popup.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public recommendedProducts = [];
  public blogPosts = [];
  subscribeForm: FormGroup;
  subscribed: boolean;
  private selectedProduct: SelectedProduct;
  quantityForm: FormGroup;
  public sizes = [];
  public actual_priceUSD: number;
  public actual_priceINR: number;
  public productDetails;

  constructor(
    private api: ApiService,
    private apiCont: ApiContractService,
    private fb: FormBuilder,
    private popup: PopupService,
    private cart: CartService
  ) {
        this.subscribeForm = new FormGroup({
          email: new FormControl('', {
          validators: [Validators.required, Validators.email],
          updateOn: 'blur'
            })
        });

    this.subscribed = false;


    this.productDetails = {
      images: [{ imgSrc: '' }],
      image_directory_url: '',
      actual_priceINR: 0,
      actual_priceUSD: 0,
      sub_category_name: '',
      version_name: '',
      product_id: '',
    }

   }



popupModal() {
    (<any>$('#popupModal')).modal('show');
  }



  subscribe() {
    if(this.subscribeForm.valid) {
      this.api.subscribe(this.subscribeForm.value.email)
      .then(res => {
        this.subscribed = true;
        setTimeout(() => {
          this.subscribeForm.reset();
          this.subscribed = false;
        }, 5000)
      })
      .catch()
    }
  }


   convertPrice(price){
    let USD_rate = 1;
    let INR_rate = 72.74;
    let converted_price = 0;
    converted_price = (price * USD_rate)/INR_rate;
    return Math.round(converted_price);
  }

  getProductDetails(id){
    this.api.getProductDetails(id)
      .then(res => {
        res.data.images.forEach((item, index) => {
          item.image_name = this.apiCont.buildUrl2(item.image_name);
        })
        this.actual_priceUSD = this.convertPrice(res.data.actual_price);
        this.actual_priceINR = res.data.actual_price;
        this.sizes = res.data.attribute_set.Socks.Size;
        this.productDetails = res.data;
      })
      .catch();
  }

  addToCart(id) {
    this.getProductDetails(id);
    let totalPriceUSD = this.convertPrice(this.actual_priceINR);
    let totalPriceINR = this.actual_priceINR;
    this.selectedProduct = {
      product_id: this.productDetails.product_id,
      size: this.sizes[0].varchar_field_value,
      size_id: this.sizes[0].Size_id,
      quantity: 1,
      quantity_id: this.productDetails.attribute_set.Socks.Quantity[0].Quantity_id,
      priceUSD: this.convertPrice(this.actual_priceINR),
      priceINR: this.actual_priceINR,
      edition: this.productDetails.sub_category_name,
      product_title: this.productDetails.version_name,
      imgSrc: this.productDetails.images[0].image_name,
      totalPriceUSD: totalPriceUSD,
      totalPriceINR: totalPriceINR,
      availSizes: this.sizes
    };
    this.cart.addToCart(this.selectedProduct);
  }



  ngOnInit() {

    this.api.getRecommededProducts()
      .then(res => {
        res.data.forEach((item, index) => {
          item.image = this.apiCont.buildUrl2(item.image);
        })
        this.recommendedProducts = res.data;
      })
      .catch()

      this.api.getBlogPosts()
      .then(res => {
        this.blogPosts = res.data
      })
      .catch()


      if(!this.popup.getPopupStatus()){
        this.popup.setPopupStatus(false);
        setTimeout(()=>{    //<<<---    using ()=> syntax
          this.popupModal();
        }, 5000);
        this.popup.setPopupStatus(true);
        setTimeout(()=>{    //<<<---    using ()=> syntax
          this.popup.setPopupStatus(false);
        }, 200000);
      }


  }

}
