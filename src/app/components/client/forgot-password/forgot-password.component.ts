import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../../services/authentication.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import * as toastr from 'toastr';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  resetForm: FormGroup;
  private activeRoute;
  public isPasswordInvalid: boolean = false;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private auth: AuthenticationService,
    private spinnerService: Ng4LoadingSpinnerService,
    private router: Router
  ) {
    this.route.params.subscribe(params => {
      this.activeRoute = params;
    })
    this.resetForm = new FormGroup({
      password: new FormControl('', {
        validators: [Validators.required, Validators.minLength(6), Validators.maxLength(15)],
        updateOn: 'blur'
      }),
      cnfrmPassword: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      })
    })
  }

  reset() {
    this.spinnerService.show();
    let value = this.resetForm.value;
    if (this.resetForm.valid) {
      if (value.password === value.cnfrmPassword) {
        let data = this.resetForm.value;
        let obj = {
          password: data.password,
          token: this.activeRoute.id
        }
        this.auth.newPassword(obj)
          .then(res => {
            this.resetForm.reset;
            this.spinnerService.hide();
            toastr.success('Password reset successful! Please login');
            this.router.navigateByUrl('/my-account');
          })
          .catch(err => {
            this.spinnerService.hide();
            toastr.error('Please try again!');
          })
      }
      else {
        this.isPasswordInvalid = true;
        setTimeout(() => {
          this.isPasswordInvalid = false;
        }, 5000)
      }
    }
  }

  ngOnInit() {
  }

}
