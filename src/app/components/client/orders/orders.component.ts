import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ApiContractService } from '../../../services/api-contract.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  public orders = [];

  constructor(
    private api: ApiService,
    private sanitizer: DomSanitizer,
    private apiCont: ApiContractService
  ) {

   }

  ngOnInit() {
    this.api.getOrders()
    .then(res => {
      res.data.forEach((item, index) => {
          item.forEach((item2, index2) => {
            item2.image = this.apiCont.buildUrl2(item2.image)
          })
      })
          
     // console.log(res.data);
      this.orders = res.data;
    })
    .catch()
  }

}
