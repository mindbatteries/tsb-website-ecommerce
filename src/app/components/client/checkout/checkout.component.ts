import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CartService } from '../../../services/cart.service';
import { ApiService } from '../../../services/api.service';
import { environment } from '../../../../environments/environment';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import * as $ from 'jquery';
import * as toastr from 'toastr';
import { empty } from 'rxjs/Observer';
import { isNgTemplate } from '@angular/compiler';
require('bootstrap');
declare var paypal,discountCode:any;

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  private didRenderPaypal: boolean = false;
  shippingForm: FormGroup;
  subscribeForm: FormGroup;
  subscribed: boolean;
  public myCart;
  public currentScreen: number;
  public shippingAddress;
  public amountPayableUSD: number;
  public amountPayableINR: number;
  public discountUSD: number;
  public discountINR: number;
  public discountValid: boolean;
  public fiveOrMoreItems =  false;
  public gstCharges: number;
  public subTotalUSD: number;
  public subTotalINR: number;
  private options;
  public miscCharges;
  public discountCode:any;
  public discountInput:any;
  private rzp1: any;
  public placeOrderBtn;
  public razorpay = false;
  public paypal = false;
  public delivery_available;
  public delivery_charges;
  public delivery_days;
  public usa_delivery_charges = 0;

  constructor(
    private fb: FormBuilder,
    private cart: CartService,
    private api: ApiService,
    private spinnerService: Ng4LoadingSpinnerService
    ) {
    this.shippingForm = new FormGroup({
      name: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      }),
      email: new FormControl('', {
        validators: [Validators.required, Validators.email],
        updateOn: 'blur'
      }),
      phone: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      }),
      address: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      }),
      city: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      }),
      state: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      }),
      country: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      }),
      pincode: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      })
    });

    this.miscCharges = {
      delivery: 30,
      gst: 5
    }

    this.placeOrderBtn = {
      show: false,
      type: ''
    }
    this.discountCode={
      name:'10PERCENTOFF',
      percentageOff:10,
      // maxDiscountInRupees:100,
      // maxDiscountInUSD:10
    }
    this.subscribeForm = new FormGroup({
      email: new FormControl('', {
        validators: [Validators.required, Validators.email],
        updateOn: 'blur'
      })
    });

    this.subscribed = false;

    this.currentScreen = 1;
    this.shippingAddress = [];
  }

  saveAddress() {
    var $this=this;
    if(this.shippingForm.invalid) return;
    let data = this.shippingForm.value;
    let address = {
      name: data.name,
      email: data.email,
      phone: data.phone,
      address: data.address,
      city: data.city,
      state: data.state,
      country: data.country,
      pincode: data.pincode
    }
    this.shippingAddress = address;
    let string = address.country.toString();
    let lowercase = string.toLowerCase();
    let substring = 'india';
    if(lowercase.includes(substring)){
      this.razorpay = true;
      this.paypal = false;
    }
    else{
      this.paypal = true;
      this.razorpay = false;
    }

    this.api.postAddress(data)
    .then(res => {
      let mydata = res.data;
      this.delivery_available = mydata.delivery_service;
      this.delivery_charges = mydata.delivery_charges;
      if(this.subTotalUSD >= 50){
          this.usa_delivery_charges = 0;
        }else{
            this.usa_delivery_charges = 15;
      }
          // this.usa_delivery_charges = 15 + (5 * Math.floor((Math.floor(this.subTotalUSD/10) -1)/4));
          this.delivery_days = mydata.delivery_days;
          if(!this.delivery_available && this.razorpay){
            this.showUnavaliableDeliveryPopup();
          }
          else{
            this.changeScreen(2);
            this.configurePaypal();
          }
        })
    .catch()



  }

  showUnavaliableDeliveryPopup() {
    (<any>$('#shipMessage')).modal('show');
  }

  paymentMode(event) {

    this.placeOrderBtn.show = true;
    this.placeOrderBtn.type = event.target.value;
  }
  applyDiscountCode(){
    if(!this.discountInput){
      alert('Please enter coupon code!');
      return;
    }
    this.api.checkCouponCodeValidity({couponCode:this.discountInput})
    .then(res => {
      this.discountCode=res.data;
      this.calPayableAmount(true);
    })
    .catch(err=>{
      console.log(err);
      this.discountValid = false;
    })
  }
  cashOnDeliveryConfirmed(){
      this.authorizePayment('cashOnDelivery',2);
      this.hideConfirmationModal();
  }
  placeOrder() {
    switch (this.placeOrderBtn.type) {
      case 'cashOnDelivery':
      (<any>$('#cashOnDeliveryConfirmationPopup')).modal('show');
      break;
      default:
      if(this.razorpay)
        this.payNow();
      break;
    }
  }

  subscribe() {
    if(this.subscribeForm.valid) {
      this.api.subscribe(this.subscribeForm.value.email)
      .then(res => {
        this.subscribed = true;
        setTimeout(() => {
          this.subscribeForm.reset();
          this.subscribed = false;
        }, 5000)
      })
      .catch()
    }
  }


  orderPlaced() {
    (<any>$('#orderPlaced')).modal('show');
  }

  convertPrice(price){
    let USD_rate = 1;
    let INR_rate = 70;
    let converted_price = 0;
    converted_price = (price * USD_rate)/INR_rate + this.usa_delivery_charges;
    return converted_price.toFixed(2);
    //  return Math.round(converted_price);
  }


  configurePaypal() {
    let amountPayableUSD = this.amountPayableUSD;
    let subTotalUSD = this.convertPrice(this.subTotalINR);
      var $this=this;
      if (!this.didRenderPaypal) {
        var userId = 2;
        this.loadPaypalScript().then(() => {
          paypal.Button.render({
            env: 'production',
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            client: {
              sandbox:    environment.services.paypal.sandbox,
              production: environment.services.paypal.production
            },
            // Show the buyer a 'Pay Now' button in the checkout flow
            commit: true,

            // payment() is called when the button is clicked
            payment: function(data, actions) {
              // Make a call to the REST api to create the payment
              return actions.payment.create({
                payment: {
                  transactions: [
                  {
                    amount: {
                      total: $this.amountPayableUSD+$this.usa_delivery_charges,
                      // total: 0.01,
                      currency: 'USD',
                      details: {
                        subtotal: $this.amountPayableUSD+$this.usa_delivery_charges,
                        // subtotal: 0.01,
                        //tax: gstCharges,
                        tax: 0,
                      }
                    },
                    custom: JSON.stringify({ // YOU CAN ADD CUSTOM DATA HERE
                      user_id: userId,
                      qty: 1
                    })
                  }
                  ]
                }
              });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function(data, actions) {
              $this.authorizePayment(data.paymentID,1);
              return actions.payment.execute().then(function() {
                console.log(data);
              });
            }

          }, '#paypal-button-container');
        });
      }
    }

    private loadPaypalScript(): Promise<any> {
      this.didRenderPaypal = true;
      return new Promise((resolve, reject) => {
        const scriptElement = document.createElement('script');
        scriptElement.src = 'https://www.paypalobjects.com/api/checkout.js';
        scriptElement.onload = resolve;
        document.body.appendChild(scriptElement);
      });
    }




    payNow() {
      this.options = {
        key: environment.Razorpay.key_id,
        amount: this.amountPayableINR * 100,
        name: environment.Razorpay.name,
        description: environment.Razorpay.description,
        image: environment.Razorpay.image,
        handler: (response) => {
          this.authorizePayment(response.razorpay_payment_id,1);
        },
        prefill: {
          name: this.shippingAddress.name,
          email: this.shippingAddress.email,
          contact: this.shippingAddress.phone
        },
        notes: {
          address: this.shippingAddress.address
        },
        theme: {
          color: environment.Razorpay.theme_color
        }
      };
      this.rzp1 = new Razorpay(this.options);
      this.rzp1.open();
    }

    changeScreen(goto) {
      if (goto == 2 && this.shippingForm.invalid) {
        return;
      }
      this.currentScreen = goto;
    }




    authorizePayment(id,paymentMode) {
      if(paymentMode==1){
        toastr.info("Please Wait...Authorizing Payment");
      }else{
        toastr.info("Please Wait...");
      }
      let array = {
        payment_mode_id: paymentMode,
        payment_id: id,
        tax_id: 1,
        //delivery_charge_id: 3,
        amount: this.subTotalINR,
        paypal: this.paypal,
        razorpay: this.razorpay,
        items: []
      };
      array.items = this.myCart.items.map((item,index) => {
        return {
          product_id: item.product_id,
          quantity_id: item.quantity_id,
          quantity: item.quantity,
          size_id: item.size_id,
          color_id: 1
        }
      })
      this.api.authorizePayment(array)
      .then(res => {
        // console.log(res.data);
        this.cart.emptyCart();
        this.orderPlaced();
      })
      .catch(err => {
        console.log("err is ->>>"+err);
      })
    }

    calPayableAmount(discount=false) {
      if(this.myCart.items.totalItems != 0){
        this.subTotalUSD = this.myCart.info.subTotalUSD;
        this.subTotalINR = this.myCart.info.subTotalINR;
        this.gstCharges = this.miscCharges.gst / 100 * this.myCart.info.subTotalINR;
        // this.amountPayable = this.subTotal + this.gstCharges;
        this.amountPayableUSD = this.subTotalUSD;
        this.amountPayableINR = this.subTotalINR;
        if(discount){
          if(this.discountCode && this.subTotalINR > 499){

            if(this.subTotalINR%10 < 6){
              this.fiveOrMoreItems = true;
            }

            if(this.discountCode.discount_code==="TSBSPL5" && this.fiveOrMoreItems == false){
                this.discountValid = false;
            }else{
              this.discountValid = true;

              if(!this.discountCode.valid_only_for_indian_orders){
                this.amountPayableUSD = Math.round(this.subTotalUSD * ((100-this.discountCode.discount_percentage)/100));
                this.discountUSD = Math.round(this.subTotalUSD * this.discountCode.discount_percentage/100);
                if(this.discountUSD>this.discountCode.max_discount_usd){
                  this.amountPayableUSD = this.subTotalUSD-this.discountCode.max_discount_usd;
                  this.discountUSD=this.discountCode.max_discount_usd;
                }
              }
              this.amountPayableINR = Math.round(this.subTotalINR * ((100-this.discountCode.discount_percentage)/100));
              this.discountINR = Math.round(this.subTotalINR * this.discountCode.discount_percentage/100);
              if(this.discountINR>this.discountCode.max_discount_inr){
                  this.amountPayableINR = this.subTotalINR-this.discountCode.max_discount_inr;
                  this.discountINR=this.discountCode.max_discount_inr;
                }
            }
          }else{
            this.discountValid = false;
          }
        }
      }else{
        this.gstCharges = 0;
        this.miscCharges.delivery = 0;
        this.amountPayableUSD = 0;
        this.amountPayableINR = 0;
      }
    }

    hideModal() {
      (<any>$('#orderPlaced')).modal('hide');
    }
    hideConfirmationModal() {
      (<any>$('#cashOnDeliveryConfirmationPopup')).modal('hide');
    }

    ngOnInit() {
      this.cart.cartStatus().subscribe(res => {
        this.myCart = res;
        this.myCart.info.subTotalUSD = this.myCart.info.subTotalUSD ? this.myCart.info.subTotalUSD : 0;
        this.myCart.info.subTotalINR = this.myCart.info.subTotalINR ? this.myCart.info.subTotalINR : 0;
        this.calPayableAmount();
      })

    }

  }
