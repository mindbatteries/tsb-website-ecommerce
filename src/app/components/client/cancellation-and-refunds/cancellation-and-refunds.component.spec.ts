import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancellationAndRefundsComponent } from './cancellation-and-refunds.component';

describe('CancellationAndRefundsComponent', () => {
  let component: CancellationAndRefundsComponent;
  let fixture: ComponentFixture<CancellationAndRefundsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancellationAndRefundsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancellationAndRefundsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
