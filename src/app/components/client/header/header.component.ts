import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../services/authentication.service';
import { CartService } from '../../../services/cart.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public loggedIn: boolean;
  public myCart;
  public currentUser;

  constructor(
    private auth: AuthenticationService,
    private cart: CartService
  ) { }

  logout() {
    this.auth.logout();
  }

  toggleMiniCart() {
    $('ul.dropdown-minicart').addClass('showCart');
    setTimeout(() => {
      $('ul.dropdown-minicart').removeClass('showCart');
    }, 3000)
  }

  ngOnInit() {
    let showCartCounter = 0;
    this.auth.loggedIn().subscribe(res => {
      this.loggedIn = res;
      this.currentUser = this.auth.getCurrentUser();
    });

    this.cart.cartStatus().subscribe(res => {
      this.myCart = res;
      if(showCartCounter > 0) {
        this.toggleMiniCart()
      }
      showCartCounter += 1;
    });


    $("#button1").click(function(event) {
      var target_div = document.getElementById("navbarSupportedContent1");
      target_div.classList.toggle("show");
    });


    $("#button2").click(function(){
      $("#navbarSupportedContent2").toggle(500);
    });

    $("#button2").mouseleave(function(){
      $("#navbarSupportedContent2").hide(500);
    });

    $(".nav-link").click(function(event) {
      var target_div = document.getElementById("navbarSupportedContent2");
      target_div.classList.toggle("show");
  });
  }

}
