import { Component, OnInit } from '@angular/core';
import { CartService } from '../../../services/cart.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public myCart;

  constructor(
    private cart: CartService
  ) { }

  deleteFromCart(index) {
    this.cart.removeFromCart(index);
  }

  changeQuantity(att, index) {
    let data;
    let value = this.myCart.items[index].quantity;
    value = value || 0;
    switch (att) {
      case 'plus':
        data = { attribute: 'quantity', value: value + 1 };
        break;
      case 'minus':
        data = { attribute: 'quantity', value: value == 0 ? 0 : value - 1 };
        break;
    }
    if(data.value != 0){
      this.cart.updateCart(index, data);
    }else{
      this.deleteFromCart(index);
    }
  }

  changeSize(event, index) {
    let data = {
      attribute: 'size',
      value: event.target.value
    };
    this.cart.updateCart(index, data)
  }

  ngOnInit() {
    this.cart.cartStatus().subscribe(res => {
      this.myCart = res;
      let totalItems: number = 0;
      this.myCart.items.forEach((item) => {
        totalItems += parseInt(item.quantity);
      })
      this.myCart.items['totalItems'] = totalItems;
    })
  }
}
