import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  subscribeForm: FormGroup;
  subscribed: boolean;

  constructor(
    private fb: FormBuilder,
    private api: ApiService
  ) {
    this.subscribeForm = new FormGroup({
      email: new FormControl('', {
        validators: [Validators.required, Validators.email],
        updateOn: 'blur'
      })
    });

    this.subscribed = false;
  }

  subscribe() {
    if(this.subscribeForm.valid) {
      this.api.subscribe(this.subscribeForm.value.email)
      .then(res => {
        this.subscribed = true;
        setTimeout(() => {
          this.subscribeForm.reset();
          this.subscribed = false;
        }, 5000)
      })
      .catch()
    }
  }



  ngOnInit() {
  }

}
