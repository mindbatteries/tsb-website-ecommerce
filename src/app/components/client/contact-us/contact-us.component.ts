import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../../services/api.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import * as toastr from 'toastr';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  contactForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    private spinnerService: Ng4LoadingSpinnerService
  ) {
    this.contactForm = new FormGroup({
      name: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      }),
      email: new FormControl('', {
        validators: [Validators.required, Validators.email],
        updateOn: 'blur'
      }),
      contact: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      }),
      message: new FormControl('', {
        validators: [Validators.required, Validators.minLength(10)],
        updateOn: 'blur'
      })
    })
  }

  public contact() {
    if (this.contactForm.invalid) return;
    this.spinnerService.show();
    let data = this.contactForm.value;
    this.api.contactForm(data)
      .then(res => {
        this.contactForm.reset();
        this.spinnerService.hide();
        toastr.success(res.data.message)
      })
      .catch(err => {
        this.spinnerService.hide();
      })
  }

  ngOnInit() {
  }

}
