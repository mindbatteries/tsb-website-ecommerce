import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.css']
})
export class FaqsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
      coll[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.maxHeight){
          content.style.maxHeight = null;
        }
        else {
          content.style.maxHeight = content.scrollHeight + "px";
        } 
      });
    }

    $('button').click(function(event) {
      var id = event.target.id;
      if(id == '') {
        id = event.target.parentElement.id;
      }
      $(`#${id} i`).toggleClass('fa-angle-down fa-angle-right');
      $(`#${id} i`).toggleClass('no-margin-rt margin-rt');
    });
  }

}
