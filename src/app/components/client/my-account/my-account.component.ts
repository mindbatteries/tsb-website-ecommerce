import { Component, OnInit } from '@angular/core';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular5-social-login';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AuthenticationService } from '../../../services/authentication.service';
import { ApiContractService } from '../../../services/api-contract.service';
import { RedirectService } from '../../../services/redirect.service';
import * as $ from 'jquery';
import * as toastr from 'toastr';
require('bootstrap');

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {

  registerForm: FormGroup;
  loginForm: FormGroup;
  forgotForm: FormGroup;
  isEmailSent: boolean;
  isInvalid: boolean = false;

  constructor(
    private auth: AuthenticationService,
    private fb: FormBuilder,
    private socialAuthService: AuthService,
    private spinnerService: Ng4LoadingSpinnerService,
    private router: Router,
    private api: ApiContractService,
    private redirect: RedirectService
  ) {

    this.isEmailSent = false;

    this.registerForm = new FormGroup({
      name: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      }),
      email: new FormControl('', {
        validators: [Validators.required, Validators.email],
        updateOn: 'blur'
      }),
      password: new FormControl('', {
        validators: [Validators.required, Validators.minLength(6), Validators.maxLength(15)],
        updateOn: 'blur'
      }),
      cnfrmPassword: new FormControl(''),
      newsletter_subscribed: new FormControl(true)
    })

    this.loginForm = new FormGroup({
      email: new FormControl('', {
        validators: [Validators.required, Validators.email],
        updateOn: 'blur'
      }),
      password: new FormControl('', {
        validators: Validators.required,
        updateOn: 'blur'
      })
    })

    this.forgotForm = new FormGroup({
      email: new FormControl('', {
        validators: [Validators.required, Validators.email]
      })
    })
  }

  public socialSignIn(socialPlatform: string) {
    this.spinnerService.show();
    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        this.auth.socialLogin(userData.token, socialPlatform)
        .then(res => {
          this.spinnerService.hide();
          if(this.redirect.isRedirectToCheckout) {
            this.router.navigate(['/checkout']);
            this.redirect.redirectToCheckout(false);
          }
          else {
            this.router.navigate(['/']);
          }
        })
        .catch(err => {
          this.spinnerService.hide();
        });
      }
    );
  }

  public register() {
    if(this.registerForm.invalid) return;
    let values = this.registerForm.value;
    if (values.password === values.cnfrmPassword) {
      this.spinnerService.show();
      this.auth.register(values)
        .then(res => {
          this.spinnerService.hide();
          let loginData = { email: values.email, password: values.password };
          this.login(loginData);
        })
        .catch(err => {
          this.spinnerService.hide();
        });
    }
    else {
      this.isInvalid = true;
      setTimeout(() => {
        this.isInvalid = false;
      }, 5000)
    }
  }

  public login(loginCreds = null) {
    if(this.loginForm.invalid && loginCreds == null) return;
    this.spinnerService.show();
    let values = this.loginForm.value;
    values = loginCreds ? loginCreds : values;
    this.auth.login(values, 'web')
      .then(res => {
        this.spinnerService.hide();
        if(this.redirect.isRedirectToCheckout) {
          this.router.navigate(['/checkout']);
          this.redirect.redirectToCheckout(false);
        }
        else {
          this.router.navigate(['/']);
        }
      })
      .catch(err => {
        if(loginCreds){
          toastr.error('An account for this email exists. Pleae login to continue');
        }else{
          toastr.error('Invalid Credentials');
        }
        this.spinnerService.hide();
      })
  }

  public forgotPassword() {
    if(this.forgotForm.invalid) return;
    let email = this.forgotForm.value.email;
    this.auth.forgotPassword(email)
      .then(res => {
        this.isEmailSent = true;
      })
      .catch()
  }

  ngOnInit() {
  }
}
