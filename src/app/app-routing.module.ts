import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/client/home/home.component';
import { MyAccountComponent } from './components/client/my-account/my-account.component';
import { ProductsPageComponent } from './components/client/products-page/products-page.component';
import { ContactUsComponent } from './components/client/contact-us/contact-us.component';
import { CareersComponent } from './components/client/careers/careers.component';
import { CartComponent } from './components/client/cart/cart.component';
import { PoliciesComponent } from './components/client/policies/policies.component';
import { FaqsComponent } from './components/client/faqs/faqs.component';
import { BlogComponent } from './components/client/blog/blog.component';
import { DynamicblogComponent } from './components/client/dynamicblog/dynamicblog.component';
import { PDescriptionPageComponent } from './components/client/p-description-page/p-description-page.component';
import { OrdersComponent } from './components/client/orders/orders.component';
import { CheckoutComponent } from './components/client/checkout/checkout.component';
import { ForgotPasswordComponent } from './components/client/forgot-password/forgot-password.component';
import { TermsAndConditionsComponent } from './components/client/terms-and-conditions/terms-and-conditions.component';
import { LoggedInGuard } from './guards/logged-in.guard';
import { CancellationAndRefundsComponent } from './components/client/cancellation-and-refunds/cancellation-and-refunds.component';
import { AboutUsComponent } from './components/client/about-us/about-us.component';
import { PaymentResultComponent } from './components/client/payment-result/payment-result.component';


import { ForgotPasswordResolveService } from './services/forgot-password-resolve.service';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'my-account',
    component: MyAccountComponent
  },
  {
    path: 'collections',
    component: ProductsPageComponent
  },
  {
    path: 'payment-status',
    component: PaymentResultComponent
  },
  {
    path: 'cart',
    component: CartComponent
  },
  {
    path: 'about',
    component: AboutUsComponent
  },
  {
    path: 'contact',
    component: ContactUsComponent
  },
  {
    path: 'careers',
    component: CareersComponent
  },
  {
    path: 'policies',
    component: PoliciesComponent
  },
  {
    path: 'tandC',
    component: TermsAndConditionsComponent
  },
  {
    path: 'faqs',
    component: FaqsComponent
  },
  {
    path: 'returns',
    component: CancellationAndRefundsComponent
  },
  {
    path: 'cancellation-refunds',
    component: CancellationAndRefundsComponent
  },
  {
    path: 'description/:id',
    component: PDescriptionPageComponent
  },
  {
    path: 'blog',
    component: BlogComponent
  },
  {
    path: 'blog/:id',
    component: DynamicblogComponent
  },
  {
    path: 'orders',
    component: OrdersComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'checkout',
    component: CheckoutComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'password/reset/:id',
    component: ForgotPasswordComponent,
    resolve: {forgotPasswordToken: ForgotPasswordResolveService}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
