import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { AppRoutingModule } from './app-routing.module';

import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider
} from 'angular5-social-login';

import { AppComponent } from './app.component';
import { LayoutComponent } from './components/client/layout/layout.component';
import { HeaderComponent } from './components/client/header/header.component';
import { HomeComponent } from './components/client/home/home.component';
import { FooterComponent } from './components/client/footer/footer.component';
import { MyAccountComponent } from './components/client/my-account/my-account.component';
import { ContactUsComponent } from './components/client/contact-us/contact-us.component';
import { ApiService } from './services/api.service';
import { AuthenticationService } from './services/authentication.service';
import { ApiContractService } from './services/api-contract.service';
import { ProductsPageComponent } from './components/client/products-page/products-page.component';
import { CareersComponent } from './components/client/careers/careers.component';
import { StorageService } from './services/storage.service';
import { CartComponent } from './components/client/cart/cart.component';
import { PoliciesComponent } from './components/client/policies/policies.component';
import { FaqsComponent } from './components/client/faqs/faqs.component';
import { PDescriptionPageComponent } from './components/client/p-description-page/p-description-page.component';
import { OrdersComponent } from './components/client/orders/orders.component';
import { ForgotPasswordComponent } from './components/client/forgot-password/forgot-password.component';
import { CartService } from './services/cart.service';
import { ForgotPasswordResolveService} from './services/forgot-password-resolve.service';
import { RedirectService} from './services/redirect.service';
import { PopupService} from './services/popup.service';
import { CheckoutComponent } from './components/client/checkout/checkout.component';
import { AnimationComponent } from './components/client/animation/animation.component';

import { LoggedInGuard } from './guards/logged-in.guard';
import { BlogComponent } from './components/client/blog/blog.component';
import { DynamicblogComponent } from './components/client/dynamicblog/dynamicblog.component';
import { TermsAndConditionsComponent } from './components/client/terms-and-conditions/terms-and-conditions.component';
import { CancellationAndRefundsComponent } from './components/client/cancellation-and-refunds/cancellation-and-refunds.component';
import { AboutUsComponent } from './components/client/about-us/about-us.component';
import { PaymentResultComponent } from './components/client/payment-result/payment-result.component';

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider("1707202782721216")
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("1068832868601-t7fljo5iu7gsrmr0m195l3ib4d5ckse3.apps.googleusercontent.com")
      },
    ]);
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    MyAccountComponent,
    ProductsPageComponent,
    ContactUsComponent,
    CartComponent,
    CareersComponent,
    PoliciesComponent,
    FaqsComponent,
    PDescriptionPageComponent,
    OrdersComponent,
    CheckoutComponent,
    ForgotPasswordComponent,
    AnimationComponent,
    BlogComponent,
    DynamicblogComponent,
    TermsAndConditionsComponent,
    CancellationAndRefundsComponent,
    AboutUsComponent,
    PaymentResultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SocialLoginModule,
    Ng4LoadingSpinnerModule.forRoot()

  ],
  providers: [
    ApiService,
    ApiContractService,
    AuthenticationService,
    StorageService,
    CartService,
    ForgotPasswordResolveService,
    LoggedInGuard,
    RedirectService,
    PopupService,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
