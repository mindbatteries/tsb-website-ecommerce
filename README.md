## Project details

Livesite: https://socksbakery.com

## Technologies
- Frontend in Angular 5
- Backend in Laravel 5
- HTML5, CSS3, Jquery

# TsbWebsite

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


